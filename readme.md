consuming promises,
creating promises,
resolution and states,
the Promises/A+ specification,
converting Node.js and other asynchronous functions styles to promises,
and recommended practices for using promises. 