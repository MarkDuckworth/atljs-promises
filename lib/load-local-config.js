var fs = require('fs');

/**
 * @function loadLocalConfig
 *
 * @description
 * Load and parse a JSON config file from the given path.
 *
 * @param configPath {string}
 * Load the JSON config from this path.
 *
 * @returns {Promise}
 * A promise that resolves with the parsed config, or rejects if the config could not be loaded or parsed.
 */
module.exports = function(configPath) {
    return new Promise(function(resolve, reject) {
        fs.readFile(configPath, 'utf8', function (error, fileContents) {
            if (!error) {
                try {
                    var config = JSON.parse(fileContents);

                    config.loadedFrom = 'file system';

                    resolve(config);
                }
                catch (e) {
                    reject("Could not parse the config.");
                }
            } else {
                reject("Could not load the config.");
            }
        });
    });
};