var request = require('request');

/**
 * @function fetchConfig
 *
 * @description
 * Fetch and parse a JSON config file from the given URL.
 *
 * @param configUrl {string}
 * Fetch the JSON config from this URL.
 *
 * @returns {Promise}
 * A promise that resolves with the parsed config, or rejects if the config could not be fetched or parsed.
 */
module.exports = function(configUrl) {
    return new Promise(function(resolve, reject) {
        request(configUrl, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                try {
                    var config = JSON.parse(body);

                    config.loadedFrom = 'server';

                    resolve(config);
                }
                catch (e) {
                    reject("Could not parse the config.");
                }
            } else {
                reject("Could not fetch the config.");
            }
        });
    });
};