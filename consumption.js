var util = require('util');

/**
 * @function fetchConfig
 *
 * @description
 * Fetch and parse a JSON config file from the given URL.
 *
 * @param configUrl {string}
 * Fetch the JSON config from this URL.
 *
 * @returns {Promise}
 * A promise that resolves with the parsed config, or rejects if the config could not be fetched or parsed.
 */
var fetchConfig = require('./lib/fetch-config.js');

var loadLocalConfig = require('./lib/load-local-config.js');


// init the app when the config is loaded
function initApp(config) {
    console.log(`Initialiing app with config:\n ${util.inspect(config)}`)
}

var configUrl = 'https://bitbucket.org/MarkDuckworth/atljs-promises/raw/4f3d7264a8991b23679d74469668eeca15aa4796/static/config.jsonx';
var configPromise = fetchConfig(configUrl);

configPromise
    .catch(function(err) {
        console.log(`Failed to fetch the config from ${configUrl}. Error: ${err}.`);

        return loadLocalConfig('./static/config.json');
    })
    .then(initApp)
    .catch(function(err) {
        console.log(`Failed to init app: ${err}.`);
    });




// elsewhere in the app access the same config object after it has been loaded
function getBackgroundColor() {
    // return fetchConfig(configUrl).then(config) {
    return configPromise.then(function(config) {
            return config.color;
        });
}

setTimeout(function() {
    getBackgroundColor()
        .then(function(backgroundColor) {
            console.log(`Background color: ${backgroundColor}`);
        });
}, 2000);